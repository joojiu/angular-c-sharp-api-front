export class Book {
    id: number;
    name: string;
    type: string;
    price: number;
    quantity: number;
}
