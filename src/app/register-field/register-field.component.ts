import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { Book } from 'src/Model/Book';
import { RegisterDataService } from '../DataService/register-data.service';
import { RegisterUpdateComponent } from '../register-update/register-update.component';

@Component({
  selector: 'app-register-field',
  templateUrl: './register-field.component.html',
  styleUrls: ['./register-field.component.scss']
})
export class RegisterFieldComponent implements OnInit {
  @Input() book: Book = new Book();
  bookList: Book[];
  modal = false;
  bookUpdate: Book;
  @ViewChild(RegisterUpdateComponent, {static: true}) myModal: RegisterUpdateComponent;

  constructor(private dataservice: RegisterDataService) { }

  ngOnInit() {
    this.GetBooks();
  }

  GetBooks() {
    this.dataservice.GetBooks().subscribe(res => {
      this.bookList = res;
    });
  }

  GetBookById(id) {
    this.dataservice.GetBookById(id).subscribe(res => {
      this.bookUpdate = res;
      this.myModal.Open(this.bookUpdate);
    });
  }

  InsertBook(book) {
    this.dataservice.AddBook(book).subscribe(res => {
      alert('Book Added Successfully!');
      console.log(res);
    });
    window.location.reload();
  }

  UpdateBook(book) {
    this.dataservice.UpdateBook(book).subscribe(res => {
      console.log('update ', res);
    });
  }

  DeleteBook(id) {
    this.dataservice.DeleteBook(id).subscribe(res => {
      console.log('delete ', res);
    });
    window.location.reload();
  }

  modalOpen() {
    this.modal = !this.modal;
  }
}
