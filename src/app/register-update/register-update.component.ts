import { Component, OnInit, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { Book } from 'src/Model/Book';

@Component({
  selector: 'app-register-update',
  templateUrl: './register-update.component.html',
  styleUrls: ['./register-update.component.scss']
})
export class RegisterUpdateComponent implements OnInit {
  @Input() modalActive;
  book?: Book;
  @Output() bookUpdated = new EventEmitter();
  opened = false;

  constructor() { }

  ngOnInit() {
    this.book = new Book();
  }

  Open(book: Book) {
    this.opened = true;
    this.book = book;
  }

  Close() {
    this.opened = false;
  }

  SaveBook() {
    this.Close();
    this.bookUpdated.emit(this.book);
  }
}
