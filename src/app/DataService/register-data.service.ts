import { Injectable } from '@angular/core';
import { Book } from 'src/Model/Book';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { ROOT_URL } from 'src/Model/Config';

@Injectable({
  providedIn: 'root'
})
export class RegisterDataService {

  constructor(private http: HttpClient) { }

  GetBooks() {
    return this.http.get<Book[]>(ROOT_URL + '/Books');
  }

  GetBookById(id) {
    return this.http.get<Book>(ROOT_URL + '/Books/' + id);
  }

  AddBook(book: Book) {
    const headers = new HttpHeaders().set('content-type', 'application/json');
    let body = {
      Name: book.name,
      Type: book.type,
      Price: book.price,
      Quantity: book.quantity
    };
    console.log(ROOT_URL);

    return this.http.post<Book>(ROOT_URL + '/Books', body, { headers });
  }

  UpdateBook(book: Book) {
    const headers = new HttpHeaders().set('content-type', 'application/json');
    let body = {
      Id: book.id,
      Name: book.name,
      Type: book.type,
      Price: book.price,
      Quantity: book.quantity
    };
    console.log(ROOT_URL);

    return this.http.put<Book>(ROOT_URL + '/Books/' + book.id, body, { headers });
  }

  DeleteBook(id) {
    return this.http.delete(ROOT_URL + '/Books/' + id);
  }
}
